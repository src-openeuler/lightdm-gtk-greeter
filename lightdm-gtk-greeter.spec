%define _libexecdir %_prefix/libexec
%define _localstatedir %_var

Name: lightdm-gtk-greeter
Version: 2.0.6
Release: 3
Summary: LightDM GTK+ Greeter
Group: Graphical desktop/Other
License: GPLv3+
#Url: https://launchpad.net/lightdm-gtk-greeter
Url: https://github.com/Xubuntu/lightdm-gtk-greeter.git
#To get source code use the command "bzr branch lp:lightdm-gtk-greeter"
Source: %{name}-%{version}.tar.gz	
#Source: %{name}_%{version}.orig.tar.gz	
#Patch1: %name-2.0.1-alt-fixes.patch
#Patch2: %name-%version-advanced.patch

Requires: lightdm >= 1.16.7-alt11
#Requires: gnome-icon-theme gnome-icon-theme-symbolic gnome-themes-standard
#Requires: gnome-icon-theme gnome-themes-standard
Requires: gnome-themes-standard
#Requires: /usr/share/design/current

Provides: lightdm-greeter

BuildRequires: autoconf
BuildRequires: gcc-c++ intltool gnome-common gobject-introspection-devel
BuildRequires: glib2-devel
BuildRequires: pkgconfig(gtk+-3.0)
BuildRequires: pkgconfig(gmodule-export-2.0)
#BuildRequires: pkgconfig(liblightdm-gobject-1) >= 1.3.5
#BuildRequires: liblightdm-gobject-1
#BuildRequires: liblightdm-gobject-devel
BuildRequires: lightdm-gobject-devel
BuildRequires: pkgconfig(x11)
BuildRequires: pkgconfig(libxklavier)
#BuildRequires: lightdm-devel >= 1.16.7-alt11 lightdm-gir-devel >= 1.16.7-alt11
#BuildRequires: /usr/bin/exo-csource
BuildRequires: exo-devel

%description
This package provides a GTK+-based LightDM greeter engine. In contrast
to the conventional "lightdm-gtk-greeter" package this version is
directly controlled by PAM prompts and messages.

%prep
%setup -n %name-%version
#%patch1 -p1
#%patch2 -p1

%build
#%autoreconf
./autogen.sh
%configure \
	%{subst_enable introspection} \
	--disable-static \
	--disable-libindicator \
	--disable-indicator-services-command \
	--enable-at-spi-command="/usr/libexec/at-spi-bus-launcher --launch-immediately" \
	--with-libxklavier \
	--enable-maintainer-mode \
	--libexecdir=%_libexecdir

%make_build

%install
%make_install DESTDIR=%buildroot install

%find_lang %name

mkdir -p %{buildroot}%{_sysconfdir}/alternatives/
ln -s %{_datadir}/xgreeters/lightdm-deepin-greeter.desktop \
  %{buildroot}%{_sysconfdir}/alternatives/lightdm-greeter

%files -f %name.lang
%{_sysconfdir}/alternatives/
%_sbindir/lightdm-gtk-greeter
%_datadir/xgreeters/lightdm-gtk-greeter.desktop
%_datadir/doc/lightdm-gtk-greeter/sample-lightdm-gtk-greeter.css
%_datadir/icons/hicolor/scalable/places/*.svg
%config(noreplace) %_sysconfdir/lightdm/lightdm-gtk-greeter.conf

%changelog
* Thu Feb 18 2021 weidong <weidong@uniontech.com> - 2.0.6-3
- Update source code package

* Fri Aug 7 2020 weidong <weidong@uniontech.com> - 2.0.6-1
- Initial release for OpenEuler
